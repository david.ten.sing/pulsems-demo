$(document).ready(function() {

    // storing canvas for rendering of chart by ChartJS
    let canvas = $('#demoChart');

    
    // Initialise the chart
    const chart = new Chart(canvas, {
        type: 'bar',
        data: {
            
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{ 
                    maxBarThickness: 60,
                    scaleLabel: {
                        display: true,
                        labelString: 'Brands'
                    },
                    stacked: true
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Count'
                    },
                    stacked: true,
                }]
            }
        },
    });

    // let chartData = new Array;
    
    // Url of data
    let demoDataUrl = '/assets/brand-data.json';

    // Variable to store brands for the autocomplete function
    // Every time an ajax call is made to GET brands and data,
    // the brand names are fed to "brands".
    let brands;

    $.ajax({
        url: demoDataUrl,
        type: 'get',
        dataType: 'json',
        beforeSend: function() {
            $('.spinner').show();
            $('#demoChart').hide();
        },  
        success: function(result) {
            $('.spinner').hide();
            $('#demoChart').show();
            chart.data = processDemoData(result.facets.group_results.buckets);
            chart.update();

            brands = chart.data.labels;

        }
    })

    $('#searchBrandInput').on('keyup', function(event) {
        showAutocomplete(brands, event);
    });
    $('#searchBrandInput').on('focus', function(event) {
        showAutocomplete(brands, event);
    });

    $('.autocompleteSuggestions').on('click', 'li', function() {
        $('#searchBrandInput').val('');
        $('.autocompleteSuggestions').hide();
        filterChartData(chart, $(this).html());
    });

    // Variable to store the last sort direction
    // On clicking sort button, the order of the dataset will be reversed
    let directionAsc = false;

    $('#sortAlphabeticallyBtn').click(function() {
        directionAsc = !directionAsc;
        sortRawData(chart, directionAsc);
    })

    $('#clearFilters').click(function(){
        clearFilter(chart);
    })
    
});

// Sort data and pass it to the chart
function sortRawData(c, sd) {
    let demoDataUrl = '/assets/brand-data.json';

    $.ajax({
        url: demoDataUrl,
        type: 'get',
        dataType: 'json',
        beforeSend: function() {
            $('.spinner').show();
            $('#demoChart').hide();
        },  
        success: function(result) {
            result.facets.group_results.buckets.sort(sorting);
            $('.spinner').hide();
            $('#demoChart').show();
            // cd = processDemoData(result);
            c.data = processDemoData(result.facets.group_results.buckets);
            c.update();

            brands = c.data.labels;

        }
    });

    let modifier;
    sd ? modifier=1 : modifier=-1;

    function sorting(a, b) {
        if(a.val < b.val)
        return -1*modifier;
        if(a.val > b.val)
        return 1*modifier;
        return 0;
    }

}

// variable to control up and down keyboard arrow
let i = -1;

function showAutocomplete(b, e) {
    var inputBox = $(e.currentTarget);
    var suggestionsCtn = $(e.currentTarget).siblings('.autocompleteSuggestions');

    var keyword = inputBox.val();

    if(keyword.length >= 3) {
        let returnedResult = filterBrands(b, keyword);
        $('.autocompleteSuggestions ul').empty();
        
        if(returnedResult.length > 0) {
            returnedResult.forEach(result => {
                $('.autocompleteSuggestions ul').append('<li>'+result+'</li>')
            })
            const limit = returnedResult.length - 1;
            if(e.keyCode === 40) {
                if(i < limit) {
                    i++;
                } else {
                    i=0
                }
                suggestionsCtn.find('ul li:eq('+i+')').css({
                    backgroundColor: '#CCC',
                });
                e.preventDefault();
            } else if(e.keyCode === 38) {
                if(i > 0) {
                    i--;
                } else {
                    i = limit;
                }
                $('.autocompleteSuggestions ul').find('li:eq('+i+')').css({
                    backgroundColor: '#CCC',
                });
                e.preventDefault();
            } else if (e.keyCode === 13) {
                $('.autocompleteSuggestions ul').find('li:eq('+i+')').click();
            }
        } else {
            $('.autocompleteSuggestions ul').append('<li>No brand found.</li>')
        }
        suggestionsCtn.show();
        
    } else {
        $('.autocompleteSuggestions ul').empty();
        suggestionsCtn.hide();
    }

}

// Function to filter out brands from the dataset to feed to the autocomplete method
function filterBrands(b, k) {
    let result = new Array;
    b.forEach(brand => {
        let brandLC = brand.toLowerCase();
        if ( brandLC.includes( k.toLowerCase() ) ) {
            result.push(brand);
        } else {
        }
    });

    return result;

}

// Function to filter the dataset based on the brand selected from the autocomplete medthod
function filterChartData(c, b) {

    let demoDataUrl = '/assets/brand-data.json';

    $.ajax({
        url: demoDataUrl,
        type: 'get',
        dataType: 'json',
        beforeSend: function() {
            $('.spinner').show();
            $('#demoChart').hide();
        },  
        success: function(result) {
            const filteredResult = new Array (result.facets.group_results.buckets.find( bucket => bucket.val === b));
            // console.log(filteredResult);
            $('.spinner').hide();
            $('#demoChart').show();
            c.data = processDemoData(filteredResult);
            c.update();

            brands = c.data.labels;

        }
    })




    // const i = c.data.labels.indexOf(b);

    // c.data.labels = new Array( c.data.labels[i] );
    // c.data.datasets.forEach(dataset => {
    //     // console.log(dataset.data[i]);
    //     dataset.data = dataset.data.slice(i, i+1);
    // })
    // c.update();
}

// Reset the dataset to its original form
function clearFilter(c) {
    let demoDataUrl = '/assets/brand-data.json';

    $.ajax({
        url: demoDataUrl,
        type: 'get',
        dataType: 'json',
        beforeSend: function() {
            $('.spinner').show();
            $('#demoChart').hide();
        },  
        success: function(result) {
            $('.spinner').hide();
            $('#demoChart').show();
            c.data = processDemoData(result.facets.group_results.buckets);
            c.update();

            brands = c.data.labels;

        }
    })
}


function processDemoData(rawData) {
    const buckets = rawData;

    console.log(buckets);

    const labels = buckets.map(brand => brand.val);

    const onTime = buckets.map( (brand) => {
        function filterOnTime(item) {
            if(item.val === 'On Time') {
                return true;
            }
            return false;
        }

        const onTimeData = brand.form_data.buckets.find(filterOnTime);

        return onTimeData ? onTimeData.count : 0;
    })

    const week1late = buckets.map( (brand) => {
        function filterOnTime(item) {
            if(item.val === '1 Week Late') {
                return true;
            }
            return false;
        }
        const onTimeData = brand.form_data.buckets.find(filterOnTime);
        return onTimeData ? onTimeData.count : 0;
    })

    const week2 = buckets.map( (brand) => {
        function filterOnTime(item) {
            if(item.val === '2 or More Weeks Late') {
                return true;
            }
            return false;
        }
        const onTimeData = brand.form_data.buckets.find(filterOnTime);
        return onTimeData ? onTimeData.count : 0;
    })

    const dataSetSource = {
        labels: labels,
        datasets: [
            {
                label: 'On Time',
                data: onTime,
                backgroundColor: '#FFC300',
            },
            {
                label: '1 Week Late',
                data: week1late,
                backgroundColor: '#DAF7A6',
            },
            {
                label: '2 or More Weeks Late',
                data: week1late,
                backgroundColor: '#FF5733',
            },
        ]
    }
    
    return dataSetSource;
}